var socket = null
var peer = null
var userId = Math.floor(Math.random() * 99999999999)
var activeRoomId = null
var type = 'camera'
var peerId = null

navigator.getUserMedia = (
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia
);

function activateCamera (el, streamId = null) {
  navigator.mediaDevices.getUserMedia(
    { video: { facingMode: "user", frameRate: { ideal: 60, max: 60 } }, audio: true }
  ).then(stream => {
    if (streamId === null) {
      const localVideo = el;
      if (localVideo) {
        localVideo.srcObject = stream;
      }
    } else if(streamId != null) {
      var call = peer.call(streamId, stream);
      call.on("stream", (remoteStream) => {
        if (el) {
          el.srcObject = remoteStream;
        }
      });
    }

    // stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));
  }); 
}

function activateScreen (el) {
  navigator.mediaDevices.getDisplayMedia({ video: true, audio: true })
  .then(stream => {
    const localVideo = el;
    if (localVideo) {
      localVideo.srcObject = stream;
    }
    
    // stream.getTracks().forEach(track => peerConnection.addTrack(track, stream));
  })
  .catch(error => {
    console.error("Error accessing screen:", error);
  });
}

function refreshRooms() {
  $("#rooms").html('')
  $(".video-container").html("")
  fetch("room-list").then((resp) => {
    return resp.json()
  }).then(data => {
    data.map(item => {
      const isMyContent = item.users.find(user => user.id == userId)
      $("#rooms").append(`
        <li>${item.id} ${isMyContent ? '<button data-room="'+item.id+'" class="endDirect">DISCONNECT</button>': '<button data-room="'+item.id+'" class="joinDirect">JOIN</button>'}</li>
      `)
      if (activeRoomId == item.id) {
        item.users?.forEach(user => {
          const isMyVideo = (user.id === userId)
          $(".video-container").append(`
            <video autoplay ${isMyVideo ? 'muted': ''} class="${isMyVideo ? 'local-video': 'remote-video'}" id="${isMyVideo ? 'local-video': 'remote-video'+user.stream}"></video>
            ${isMyVideo ? '<span style="display: inline;"><div style="display: flex;flex-direction: column;align-items:flex-start;justify-content: flex-start"><span>CURRENT VIDEO TYPE: '+type+'</span><button type="button" class="video-type">toggle tipe (share screen / camera)</button></div></span>':''}
          `)
          // peer.
          if (!isMyVideo) {
            activateCamera(document.getElementById("remote-video"+user.stream), user.stream)
          } else {
            if (!type || type === 'camera') {
              activateCamera(document.getElementById("local-video"))
            } else {
              activateScreen(document.getElementById("local-video"))
            }
          }
        });
      }
    })
  })
}
document.addEventListener("DOMContentLoaded", () => {
  socket = io("wss://192.168.1.17:9000", {transports: ["websocket"]});
  peer = new Peer(undefined, {
    host: '192.168.1.17',
    port: 8005,
    path: '/peer-app',
    debug: 1,
    secure: true
  });
  
  const roomIdInput = document.getElementById("roomIdInput");
  const joinButton = document.getElementById("joinButton");
  const createButton = document.getElementById("createButton");
  const roomIdDisplay = document.getElementById("roomId");
  const userList = document.getElementById("userList");
  const roomUl = document.getElementById("rooms");

  createButton.addEventListener("click", () => {
    fetch("https://192.168.1.17:9000/create-room", {
      method: "POST", 
      body: JSON.stringify({id: userId}), 
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
      }
    }).then((resp) => {
      return resp.json()
    }).then(data => {
      socket.emit("join-room", data.id, userId, peerId);
      activeRoomId = data.id
      $('#roomId').text(data.id)
      refreshRooms()
    })
  })

  joinButton.addEventListener("click", () => {
    const roomId = roomIdInput.value.trim();

    if (roomId !== "") {
      // Emit a "join-room" event to join the specified room
      socket.emit("join-room", roomId, userId, peerId);
      activeRoomId = $(this).data('room')
      $('#roomId').text(roomId)
      refreshRooms()
    }
  });


  peer.on('open', function(id) { 
    peerId = id
  });

  socket.on("update-user-list", (users) => {
      $("#userList").html("");
      users.map(item => {
        $("#userList").append(`<li>${item.id}</li>`);
      })
  });

  $("#rooms").on("click", ".joinDirect", function() {
    socket.emit("join-room", $(this).data('room'), userId, peerId);
    activeRoomId = $(this).data('room')
    $('#roomId').text($(this).data('room'))
    refreshRooms()
  })

  $("#rooms").on("click", ".endDirect", function() {
    socket.emit("exit-room", $(this).data('room'), userId);
    activeRoomId = null
    $('#roomId').text(null)
    refreshRooms()
  })

  $(".video-container").on("click", ".video-type", function() {
    type = (type === 'camera' ? 'screen': 'camera')
    refreshRooms()
  })

  peer.on("call", function (call) {
    navigator.mediaDevices.getUserMedia(
      { video: { facingMode: "user", frameRate: { ideal: 60, max: 60 } }, audio: true }
    ).then(stream => {
      call.answer(stream);
    });
    call.on("stream", (remoteStream) => {
      console.log("current")
      const v = document.createElement('video')
      v.autoplay = true
      $(".video-container").append(v)
      v.srcObject = remoteStream
    })
  }); 

  // You can add other event handlers here as needed
  refreshRooms()
});
