export type Room = {
  id: string;
  users: Array<User>;
}
export type User = {
  socket: string;
  stream?: string;
  id: string;
}