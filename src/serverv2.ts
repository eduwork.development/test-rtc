import {readFileSync} from 'fs'
import express from "express";
import { Server as SocketIOServer } from "socket.io";
import { createServer, Server as HTTPServer } from "http";
import { createServer as createHttpsServer, Server as HTTPSServer } from "https";
import * as path from 'path'
import { v4 as uuidv4 } from 'uuid';
import {Server as WSServer} from 'ws'
import { Room, User } from "./serverv2-type";
import {PeerServer} from 'peer'
import cors from 'cors'

const ssl = {
  key: readFileSync('/srv/ssl/key.pem').toString(),
  cert: readFileSync('/srv/ssl/cert.pem').toString(),
}

const app = express();
// Serve other static files if needed
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(express.json()) // for parsing application/json
// app.set('trust proxy', true)
app.use(cors())

const httpsServer = createHttpsServer(ssl, app).listen(9000);
const httpServer = createServer(app);


const io = new SocketIOServer(httpsServer, {
  wsEngine: WSServer,
  cors: {
    origin: ["http://localhost:8004", "http://192.168.1.17:8004", "https://192.168.1.17:9000", "https://localhost:9000", "https://4c25-125-163-151-239.ngrok-free.app","https://edudev.xyz:8004","http://edudev.xyz:8004"],
    methods: ["GET", "POST"],
  },
  allowEIO3: false,
  transports: ["websocket"]
});

let rooms: Array<Room> = []; // To store room information

io.on("connection", socket => {
  socket.on("join-room", (roomId, userId, streamId) => {
    socket.join(roomId);

    const existingRoom = rooms.findIndex((room: Room) => room.id === roomId)
    if (existingRoom > -1) {
      const updatedUsers = [...rooms[existingRoom].users.filter((item: User) => item.id != userId), {id: userId, socket: socket.id, stream: streamId}]
      rooms = rooms.map((item: Room) => {
        const newFilteredUsers = item.users.filter((item: User) => item.id != userId)
        return {...item, users: newFilteredUsers}
      })
      rooms.splice(existingRoom, 1, {id: roomId, users: updatedUsers})
      socket.emit("update-user-list", updatedUsers);
    }
  });

  socket.on("disconnect", (id) => {
    rooms.map((room: Room, roomIndex) => {
      room.users.map(user => {
        if (user.socket == socket.id) {
          const updatedUsers = [...rooms[roomIndex].users.filter((item: User) => item.id != user.id)]
          rooms.splice(roomIndex, 1, {id: room.id, users: updatedUsers})
          socket.emit("update-user-list", updatedUsers);  
        }
      })
      if (room.users.length === 0) {
        rooms.splice(roomIndex, 1)
      }
    })
    console.log(socket.id+" disconnected from wss")
  });

  socket.on("exit-room", (roomId, userId) => {
    const existingRoom = rooms.findIndex((room: Room) => room.id === roomId)
    if (existingRoom > -1) {
      const updatedUsers = [...rooms[existingRoom].users.filter((item: User) => item.id != userId)]
      rooms.splice(existingRoom, 1, {id: roomId, users: updatedUsers})
      socket.emit("update-user-list", updatedUsers);
    }
  })

  console.log(`User ${socket.id} connected to wss`);
});

app.post("/create-room", (req, res) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const roomId = uuidv4();
  rooms.splice(rooms.length, 0, {id: roomId, users: []})

  res.json({id: roomId, rooms: rooms, user_id: ip});
});

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/indexv2.html"));
});

app.get("/room-list", (req, res) => {
  res.json(rooms);
});

app.use(express.static(path.join(__dirname, "../public")));

httpServer.listen(8004, "0.0.0.0", () => {
  const addr = httpServer.address() as Record<any,any>
  console.log(`Server is listening on ${addr?.address as string}:${8004}`);
});

const peerServer = PeerServer({ 
  port: 8005, 
  path: "/peer-app", 
  corsOptions: {
    origin: ["http://localhost:8004", "http://192.168.1.17:8004", "https://192.168.1.17:9000", "https://localhost:9000", "https://4c25-125-163-151-239.ngrok-free.app","https://edudev.xyz:8004"],
  },
  ssl: {...ssl}
});
peerServer.on("connection", socket => {
  peerServer.on("disconnect", socket => {
    console.log(socket.getId()+" disconnected from stream")
  })

  console.log(`User ${socket.getId()} connected to stream`)
})