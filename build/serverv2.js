"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const express_1 = __importDefault(require("express"));
const socket_io_1 = require("socket.io");
const http_1 = require("http");
const https_1 = require("https");
const path = __importStar(require("path"));
const uuid_1 = require("uuid");
const ws_1 = require("ws");
const peer_1 = require("peer");
const cors_1 = __importDefault(require("cors"));
const ssl = {
    key: (0, fs_1.readFileSync)('/srv/ssl/key.pem').toString(),
    cert: (0, fs_1.readFileSync)('/srv/ssl/cert.pem').toString(),
};
const app = (0, express_1.default)();
// Serve other static files if needed
app.use(express_1.default.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express_1.default.json()); // for parsing application/json
// app.set('trust proxy', true)
app.use((0, cors_1.default)());
const httpsServer = (0, https_1.createServer)(ssl, app).listen(9000);
const httpServer = (0, http_1.createServer)(app);
const io = new socket_io_1.Server(httpsServer, {
    wsEngine: ws_1.Server,
    cors: {
        origin: ["http://localhost:8004", "http://192.168.1.17:8004", "https://192.168.1.17:9000", "https://localhost:9000", "https://4c25-125-163-151-239.ngrok-free.app", "https://edudev.xyz:8004", "http://edudev.xyz:8004"],
        methods: ["GET", "POST"],
    },
    allowEIO3: false,
    transports: ["websocket"]
});
let rooms = []; // To store room information
io.on("connection", socket => {
    socket.on("join-room", (roomId, userId, streamId) => {
        socket.join(roomId);
        const existingRoom = rooms.findIndex((room) => room.id === roomId);
        if (existingRoom > -1) {
            const updatedUsers = [...rooms[existingRoom].users.filter((item) => item.id != userId), { id: userId, socket: socket.id, stream: streamId }];
            rooms = rooms.map((item) => {
                const newFilteredUsers = item.users.filter((item) => item.id != userId);
                return Object.assign(Object.assign({}, item), { users: newFilteredUsers });
            });
            rooms.splice(existingRoom, 1, { id: roomId, users: updatedUsers });
            socket.emit("update-user-list", updatedUsers);
        }
    });
    socket.on("disconnect", (id) => {
        rooms.map((room, roomIndex) => {
            room.users.map(user => {
                if (user.socket == socket.id) {
                    const updatedUsers = [...rooms[roomIndex].users.filter((item) => item.id != user.id)];
                    rooms.splice(roomIndex, 1, { id: room.id, users: updatedUsers });
                    socket.emit("update-user-list", updatedUsers);
                }
            });
            if (room.users.length === 0) {
                rooms.splice(roomIndex, 1);
            }
        });
        console.log(socket.id + " disconnected from wss");
    });
    socket.on("exit-room", (roomId, userId) => {
        const existingRoom = rooms.findIndex((room) => room.id === roomId);
        if (existingRoom > -1) {
            const updatedUsers = [...rooms[existingRoom].users.filter((item) => item.id != userId)];
            rooms.splice(existingRoom, 1, { id: roomId, users: updatedUsers });
            socket.emit("update-user-list", updatedUsers);
        }
    });
    console.log(`User ${socket.id} connected to wss`);
});
app.post("/create-room", (req, res) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const roomId = (0, uuid_1.v4)();
    rooms.splice(rooms.length, 0, { id: roomId, users: [] });
    res.json({ id: roomId, rooms: rooms, user_id: ip });
});
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "../public/indexv2.html"));
});
app.get("/room-list", (req, res) => {
    res.json(rooms);
});
app.use(express_1.default.static(path.join(__dirname, "../public")));
httpServer.listen(8004, "0.0.0.0", () => {
    const addr = httpServer.address();
    console.log(`Server is listening on ${addr === null || addr === void 0 ? void 0 : addr.address}:${8004}`);
});
const peerServer = (0, peer_1.PeerServer)({
    port: 8005,
    path: "/peer-app",
    corsOptions: {
        origin: ["http://localhost:8004", "http://192.168.1.17:8004", "https://192.168.1.17:9000", "https://localhost:9000", "https://4c25-125-163-151-239.ngrok-free.app", "https://edudev.xyz:8004"],
    },
    ssl: Object.assign({}, ssl)
});
peerServer.on("connection", socket => {
    peerServer.on("disconnect", socket => {
        console.log(socket.getId() + " disconnected from stream");
    });
    console.log(`User ${socket.getId()} connected to stream`);
});
