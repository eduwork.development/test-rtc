"use strict";
// import express, { Application } from "express";
// import { Server as SocketIOServer } from "socket.io";
// import { createServer, Server as HTTPServer } from "http";
// import * as path from 'path'
// import {Server as WSServer} from 'ws'
// const app = express();
// const httpServer = createServer(app);
// const io = new SocketIOServer(httpServer, {
//   wsEngine: WSServer,
//   cors: {
//     origin: "http://localhost:8004",
//     methods: ["GET", "POST"],
//     credentials: true
//   },
//   allowEIO3: false,
//   transports: ["websocket"]
// });
// let activeSockets: string[] = [];
// io.on("connection", socket => {
//   const existingSocket = activeSockets.find(
//     existingSocket => existingSocket === socket.id
//   );
//   if (!existingSocket) {
//     activeSockets.push(socket.id);
//     socket.emit("update-user-list", {
//       users: activeSockets.filter(
//         existingSocket => existingSocket !== socket.id
//       )
//     });
//     socket.broadcast.emit("update-user-list", {
//       users: [socket.id]
//     });
//   }
//   socket.on("call-user", (data: any) => {
//     socket.to(data.to).emit("call-made", {
//       offer: data.offer,
//       socket: socket.id
//     });
//   });
//   socket.on("make-answer", data => {
//     socket.to(data.to).emit("answer-made", {
//       socket: socket.id,
//       answer: data.answer
//     });
//   });
//   socket.on("reject-call", data => {
//     socket.to(data.from).emit("call-rejected", {
//       socket: socket.id
//     });
//   });
//   socket.on("disconnect", () => {
//     activeSockets = activeSockets.filter(
//       existingSocket => existingSocket !== socket.id
//     );
//     socket.broadcast.emit("remove-user", {
//       socketId: socket.id
//     });
//   });
//   console.log(activeSockets)
// })
// io.on("disconnect", socket => {
//   activeSockets = activeSockets.filter(
//     existingSocket => existingSocket !== socket.id
//   );
//   socket.broadcast.emit("remove-user", {
//     socketId: socket.id
//   });
//   console.log(activeSockets)
// });
// // app.get("/", (req, res) => {
// //   res.send(`<h1>Hello World</h1>`); 
// // });
// app.use(express.static(path.join(__dirname, "../public")));
// httpServer.listen(8004, () => {
//   console.log(`Server is listening on http://localhost:${8004}`);
// });
