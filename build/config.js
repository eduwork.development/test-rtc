"use strict";
var fs = require("fs");
module.exports = {
    cert: fs.readFileSync(__dirname + "/srv/ssl/localhost+2.pem"),
    key: fs.readFileSync(__dirname + "/srv/ssl/localhost+2-key.pem"),
    passphrase: "12345"
};
